let ById = function (id) {
    return document.getElementById(id);
}

let ByClass = function (classs) {
    return document.getElementsByClassName(classs);
}
let jsonfile = require('jsonfile');
let favicon = require('favicon-getter').default;
let path = require('path');
let uuid = require('uuid');
//let bookmarks = fs.readFileSync(__dirname + '/bookmarks.json','utf8');
let bookmarks = path.join(__dirname + '/db/bookmarks.json')
let openSite = path.join(__dirname + '/db/openSite.json')

let back = ById('back'),
    forward = ById('forward'),
    refresh = ById('refresh'),
    omni = ById('url'),
    dev = ById('console'),
    fave = ById('fave'),
    list = ById('list'),
    popup = ById('fave-popup'),
    view = ById('view');
    tabs_ = ById('tabs_');

let remove = ByClass('close');

function reloadView () {
    view.reload();
}

function backView () {
    view.goBack();
}

function forwardView () {
    view.goForward();
}

function updateURL (event) {
    if (event.keyCode === 13) {
        omni.blur();
        let val = omni.value;
        let https = val.slice(0, 8).toLowerCase();
        let http = val.slice(0, 7).toLowerCase();
        if (https === 'https://') {
            view.loadURL(val);
        } else if (http === 'http://') {
            view.loadURL(val);
        } else {
        view.loadURL(`http://${val}`);
        }
    }
}

const Bookmark = function (id, url, faviconUrl, title) {
    this.id = id;
    this.url = url;
    this.icon = faviconUrl;
    this.title = title;
}

const TabBar = function(id, url, faviconUrl, title) {
    this.id = id;
    this.url = url;
    this.icon = faviconUrl;
    this.title = title;
}

TabBar.prototype.ELEMENT = function () {
    let a_tag = document.createElement('a')
    a_tag.href = this.url;
    a_tag.className = 'tab';
    a_tag.textContent = this.title;
    a_tag.href = this.url;
    a_tag.className = 'tab';
    a_tag.textContent = this.title;
    let favimage = document.createElement('img');
    favimage.src = this.icon;
    favimage.className = 'favicon';

    let div = document.createElement('div');
    div.className = "alert";
    div.innerHTML = '<i class="fa fa-times"></i>'

    

    a_tag.insertBefore(favimage, a_tag.childNodes[0]);
    a_tag.append(div)

    return a_tag;
}

Bookmark.prototype.ELEMENT = function () {
    let a_tag = document.createElement('div');
    a_tag.href = this.url;
    a_tag.className = 'link';
    a_tag.textContent = this.title.substring(0,14);
    let favimage = document.createElement('img');
    favimage.src = this.icon;
    favimage.className = 'favicon';
   

    let div = document.createElement('button');
    div.className = "close";
    div.innerHTML = '<i class="fa fa-times"></i>';

    div.addEventListener('click', () => removeBookmark(this));

     a_tag.insertBefore(favimage, a_tag.childNodes[0]);
     a_tag.append(div)
    return a_tag;
}

function removeBookmark(item){
   jsonfile.readFile(bookmarks,function(err,curr) {
        curr = curr.filter(x => x.id !== item.id)
       jsonfile.writeFile(bookmarks,curr,function(err) {})
   })
   openPopUp()

}

function addBookmark () {
    let url = view.src;
    let title = view.getTitle();

    favicon(url).then(function(fav) {
        let book = new Bookmark(uuid.v1(), url, fav, title);
        
        jsonfile.readFile(bookmarks, function(err, curr) {

            let checkIfHaveAlready = curr.filter(x => x.url === url)

            if(checkIfHaveAlready.length === 0){
                curr.push(book)
                jsonfile.writeFile(bookmarks,curr,function(err) {

                })
                openPopUp()
            }else{
                alert('You have already')
            }
        })
    })
}

function tabs(){
    tabs_.innerHTML = '';
   jsonfile.readFile(openSite, function(err, obj) {
            if(err){
                console.log('err',err)
            }
            if(obj.length !== 0) {
                obj.map(x => {
                   if(x.length !== 0) {
                       let { id,url,icon,title } = x;
                       let tabs = new TabBar(id,url,icon,title);
                       let el  = tabs.ELEMENT();
                       tabs_.appendChild(el);
                   }
               })
            }
             
        });

}
tabs();
function openPopUp (event) {
    console.log('event',event)
    let state = popup.getAttribute('data-state');
    if (state === 'closed') {
        popup.innerHTML = '';
        jsonfile.readFile(bookmarks, function(err, obj) {
            if(err){
                console.log('err',err)
            }
            if(obj.length !== 0) {
                obj.map(x => {
                   if(x.length !== 0) {
                       let { id,url,icon,title } = x;
                       let bookmark = new Bookmark(id,url,icon,title);
                       let el  = bookmark.ELEMENT();
                       popup.appendChild(el);
                   }
               })
            }
                popup.style.display = 'block';
                popup.setAttribute('data-state', 'open');
        });
   
    } else {
        popup.style.display = 'none';
        popup.setAttribute('data-state', 'closed');
    }
}

function handleUrl (event) {
    if (event.target.className === 'link') {
        event.preventDefault();
        view.loadURL(event.target.href);
    } else if (event.target.className === 'favicon') {
        event.preventDefault();
        view.loadURL(event.target.parentElement.href);
    }
}

function handleDevtools () {
    if (view.isDevToolsOpened()) {
        view.closeDevTools();
    } else {
        view.openDevTools();
    }
}

function updateNav (event) {
    omni.value = view.src;
}


Array.from(remove).forEach(function(element) {
      element.addEventListener('click', alert('remove'));
    });

refresh.addEventListener('click', reloadView);
back.addEventListener('click', backView);
forward.addEventListener('click', forwardView);
omni.addEventListener('keydown', updateURL);
fave.addEventListener('click', addBookmark);
list.addEventListener('click', openPopUp);
popup.addEventListener('click', handleUrl);
dev.addEventListener('click', handleDevtools);
view.addEventListener('did-finish-load', updateNav);

const electron = require('electron');
const path = require('path');
const url = require('url');
const {BrowserWindow, ipcMain, Menu } = require('electron')

const { template } = require('./template')

const app = electron.app;
app.disableHardwareAcceleration()

let mainWindow


 createWindow = () => {
	mainWindow = new BrowserWindow({
		frame:false,
		webPreferences: {
			nodeIntegration: true,
			webviewTag: true,

		}
	});
	//mainWindow.setMenuBarVisibility(false)
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, './views/index.html'),
		protocol: 'file',
		slashes:  true
	}))

	mainWindow.on('closed', function () {
		mainWindow = null
	})
}

app.on('ready',createWindow)

app.on('window-all-closed', function() {
	if (process.platform !== 'darwin') {
		app.quit()
	}

})

app.on('activate',function() {
	if(mainWindow === null) {
		createWindow()
	}
})


ipcMain.on('display-app-menu', (event, arg) => {
  const appMenu = Menu.buildFromTemplate(template)
  if(mainWindow) {
    appMenu.popup(mainWindow, arg.x, arg.y)
  }
})

#AllBlue
## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Difficulties](#difficulties)
* [Bakery](#bakery)


## General info
This application is hosted in Heroku https://bakery-crm.herokuapp.com/, and in the total until the push of this readme i took 7 hours ,all application is working but after i need to create some validations and maybe create ecommerce with cleaner code now that i undestand the typescript


##Technologies
Project is created with

* Electron
* Node Js


## Setup

To run this project, install it locally using npm:

```
$ cd ./client && npm i
$ cd ./server && npm i
$ npm start
```

## Difficulties

The only difficulty I had was with typescript because I had only used typescript a long time ago and when mistakes came up I didn't know why but now I know everything has to have a type 
It's like in life it's either woman or man or both :)

## Bakery
![Homepage](./ProjectPictures/HomePage.jpg  "Homepage")
![Customers](./ProjectPictures/Customers.jpg  "Customers")
![Products](./ProjectPictures/Products.jpg  "Order")
![Order](./ProjectPictures/Order.jpg  "Order")



